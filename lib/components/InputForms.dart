import 'package:flutter/material.dart';

class InputForms extends StatefulWidget {
  bool obscure;
  String label;
  TextEditingController controller;

  InputForms({this.obscure = false, @required this.label, this.controller});
  @override
  _InputFormsState createState() => _InputFormsState(
      obscure: this.obscure, label: this.label, controller: this.controller);
}

class _InputFormsState extends State<InputForms> {
  TextEditingController controller;
  _InputFormsState({@required obscure, label, this.controller});

  bool obs = false;
  @override
  Widget build(BuildContext context) {
    return TextField(
        obscureText: this.obs,
        decoration: InputDecoration(
            prefixIcon: _btnIcon(),
            labelText: widget.label,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)))));
  }

  Widget _btnIcon() {
    if (widget.obscure) {
      return IconButton(
        onPressed: _contrasenia,
        icon: Icon(Icons.lock),
      );
    }
    return Icon(Icons.email);
  }

  void _contrasenia() {
    setState(() {
      this.obs = !this.obs;
    });
  }
}
